package com.cdac.controller;

import java.util.ArrayList;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.cdac.entity.Customer;
import com.cdac.model.LoginModel;
import com.cdac.service.CustomerService;

@Controller
public class LoginController {

	@Autowired
	CustomerService customerService;

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String loginPage(@ModelAttribute("loginModel") LoginModel loginModel) {
		loginModel.setEmail("shivamp@cdac.in");
		loginModel.setPassword("shivamp@@123");

		return "loginPage";
	}

	@RequestMapping(value = "/authenticate", method = RequestMethod.POST)
	public String authenticate(LoginModel loginModel, HttpSession session) {
		Customer customer = customerService.getCustomerByEmail(loginModel.getEmail());
		if (customer != null && customer.getPassword().equals(loginModel.getPassword())) {
			session.setAttribute("customer", customer);
			session.setAttribute("cart", new ArrayList<Integer>());
			return "redirect:getSubjects";
		} else
			return "loginFailed";
	}

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logout(HttpSession session, Model model) {
		model.addAttribute("customer", session.getAttribute("customer"));
		session.invalidate();
		return "logout";

	}

}
