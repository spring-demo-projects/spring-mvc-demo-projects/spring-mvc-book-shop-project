package com.cdac.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.cdac.entity.Book;
import com.cdac.model.BookModel;
import com.cdac.model.SubjectModel;
import com.cdac.service.BookService;

@Controller
public class BookController {

	@Autowired
	BookService bookService;

	@RequestMapping(value = "/getSubjects", method = RequestMethod.GET)
	public String getSubjects(Model model) {
		List<String> subjectList = bookService.findSubjects();
		model.addAttribute("subjectList", subjectList);

		SubjectModel defaultSelectedSubjectModel = new SubjectModel(subjectList.get(0));

		model.addAttribute("command", defaultSelectedSubjectModel);
		return "subjects";
	}

	@RequestMapping(value = "/getBooks", method = RequestMethod.POST)
	public String getBooks(SubjectModel selectedSubjectModel, Model model) {
		String subject = selectedSubjectModel.getSubject();
		List<Book> bookList = bookService.findBooksBySubject(subject);
		model.addAttribute("bookList", bookList);

		BookModel defaultSelectedBookModel = new BookModel();
		model.addAttribute("command", defaultSelectedBookModel);
		return "books";
	}

	@RequestMapping(value = "/addToCart", method = RequestMethod.POST)
	public String addToCart(BookModel bookModel, HttpSession session) {
		List<Integer> cart = (List<Integer>) session.getAttribute("cart");
		String[] bookList = bookModel.getBook();
		for (String bookId : bookList) {
			cart.add(Integer.parseInt(bookId));
		}
		return "redirect:getSubjects";
	}

	@RequestMapping(value = "/showCart", method = RequestMethod.GET)
	public String showCart(Model model, HttpSession session) {
		List<Integer> bookIdList = (List<Integer>) session.getAttribute("cart");
		List<Book> bookList = new ArrayList<Book>();
		for (Integer bookId : bookIdList) {
			Book book = bookService.findBook(bookId);
			bookList.add(book);
		}
		model.addAttribute("bookList", bookList);
		return "cart";
	}

	@RequestMapping(value = "/details", method = RequestMethod.GET)
	public String showBookDetails(@RequestParam("id") Integer id, Model model) {
		Book book = bookService.findBook(id);
		model.addAttribute("book", book);
		return "bookDetails";

	}
}
