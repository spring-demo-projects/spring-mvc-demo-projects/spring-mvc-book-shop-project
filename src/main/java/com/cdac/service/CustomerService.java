package com.cdac.service;

import java.util.List;

import com.cdac.entity.Customer;

public interface CustomerService {
	
	/*
	 * Find Customer By Id
	 */
	Customer getCustomerById(int id);

	/*
	 * Find Customer By Email
	 */
	Customer getCustomerByEmail(String email);

	/*
	 * Find All Customers
	 */
	List<Customer> getAllCustomers();

	/*
	 * Add new Customer
	 */

	void addCustomer(Customer cust);

}
