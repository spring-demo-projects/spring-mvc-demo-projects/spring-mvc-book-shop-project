package com.cdac.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cdac.dao.CustomerDao;
import com.cdac.entity.Customer;

@Service
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	CustomerDao customerDao;

	/*
	 * If we don't mentioned @Transactional annotation then we will get error :
	 *
	 * Exception in thread "main" org.hibernate.HibernateException: Could not obtain
	 * transaction-synchronized Session for current thread
	 */

	@Override
	@Transactional
	public Customer getCustomerById(int id) {
		return customerDao.findById(id);
	}

	@Override
	@Transactional
	public Customer getCustomerByEmail(String email) {
		return customerDao.findByEmail(email);
	}

	@Override
	@Transactional
	public List<Customer> getAllCustomers() {
		return customerDao.findAll();
	}

	@Override
	@Transactional
	public void addCustomer(Customer customer) {
		customerDao.addCustomer(customer);

	}

}
