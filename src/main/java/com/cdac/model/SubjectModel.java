package com.cdac.model;

public class SubjectModel {

	private String subject;

	public SubjectModel() {
		// TODO Auto-generated constructor stub
	}

	public SubjectModel(String subject) {
		super();
		this.subject = subject;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	@Override
	public String toString() {
		return "SubjectModel [subject=" + subject + "]";
	}

}
