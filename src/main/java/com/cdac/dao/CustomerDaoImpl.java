package com.cdac.dao;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.cdac.entity.Customer;

@Repository
public class CustomerDaoImpl implements CustomerDao {

	/*
	 * LocalSessionFactory class is inherited from SessionFactory class so there
	 * type is same.
	 * 
	 * In case of only hibernate we have written our own HBUtil class which handles
	 * sessionFactory
	 */
	@Autowired
	private SessionFactory sessionFactory;

	/*
	 * Create Session object From Session Factory for every new Query.
	 */

	/*
	 * Find Customer By Id
	 */
	@Override
	public Customer findById(int id) {
		Customer cust = null;

		// Get Session Factory Object and Create new Session Object from it
		// Use try with resource to ensure Session Object will close automatically

		/*
		 * Earlier in hibernate we have session from Thread session context and we don't
		 * need to open or close it. Similarly here getCurrentSession() gives us session
		 * from Spring's own 'session context' and that's why we haven't mentioned
		 * <property name="hibernate.current_session_context_class">thread</property>
		 * tag in hibernate configuration file.
		 */
		Session session = sessionFactory.getCurrentSession();
		cust = session.get(Customer.class, id);
		return cust;
	}

	@Override
	public Customer findByEmail(String email) {
		Session session = sessionFactory.getCurrentSession();
		// Create Query Object from Session Object
		Query<Customer> query = session.createQuery("FROM Customer WHERE email = :email");
		query.setParameter("email", email);
		return query.getSingleResult();
	}

	/*
	 * Find All Customers
	 */
	@Override
	public List<Customer> findAll() {
		List<Customer> list = null;

		Session session = sessionFactory.getCurrentSession();
		Query<Customer> query = session.createQuery("from Customer");
		list = query.getResultList();
		return list;

	}

	@Override
	public void addCustomer(Customer cust) {
		Session session = sessionFactory.getCurrentSession();
		Serializable primaryKey = session.save(cust);
	}
}
