package com.cdac.dao;

import java.util.List;

import com.cdac.entity.Customer;

public interface CustomerDao {

	/*
	 * Find Customer By Id
	 */
	Customer findById(int id);

	/*
	 * Find Customer By Email
	 */
	Customer findByEmail(String email);

	/*
	 * Find All Customers
	 */
	List<Customer> findAll();

	/*
	 * Add new Customer
	 */

	void addCustomer(Customer cust);

}