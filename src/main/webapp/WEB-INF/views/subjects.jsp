<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="springForm"
	uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Subjects</title>
</head>
<body>

	<h2>Hello, ${customer.name}</h2>

	<div>
		<springForm:form action="getBooks">
			<springForm:radiobuttons path="subject" items="${subjectList}"
				delimiter="<br>" />
			<br>
			<input type="submit" value="Show Books">
			<a href="showCart">Show Cart</a>
		</springForm:form>
	</div>
</body>
</html>
