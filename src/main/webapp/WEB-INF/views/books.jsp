<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="springForm"
	uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Subjects</title>
</head>
<body>

	<h2>Hello, ${customer.name}</h2>

	<div>
		<springForm:form action="addToCart">
			<c:forEach items="${bookList}" var="b">
				<springForm:checkbox path="book" value="${b.id}" label="${b.name}" />
				<a href="details?id=${b.id}">Details</a>
				<br>
			</c:forEach>

			<%-- 
			<springForm:checkboxes path="book" items="${bookList}" itemLabel="name" itemValue="id" delimiter="<br>" /> 
			--%>
			<br>
			<input type="submit" value="Add To Cart">
			<a href="showCart">Show Cart</a>
		</springForm:form>
	</div>
</body>
</html>
