<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib uri="http://www.springframework.org/tags/form"
	prefix="springForm"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>

	<div>
		<!-- Default method for Spring Form tag is POST-->
		<springForm:form action="authenticate" modelAttribute="loginModel">
			<springForm:input path="email" />
			<springForm:input path="password" />
			<input type="submit" value="Login" />
		</springForm:form>
	</div>


</body>
</html>